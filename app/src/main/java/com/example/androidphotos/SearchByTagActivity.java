package com.example.androidphotos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.FileDescriptor;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SearchByTagActivity extends AppCompatActivity {

    public static final int VIEW_PHOTO_CODE = 24;

    public UserData udata;
    private Context context = this;

    private int currentAlbumIndex;
    private int currentPhotoIndex;
    String currentAlbumName;

    Button buttonSearch, switchAndOr, switchTags;

    Spinner tagKey1, tagKey2;

    EditText tagVal1, tagVal2;

    LinearLayout linearLayout;
    ListView listView, listView1;

    ArrayList<Photo> photosResult;

    //this is used to store each photo's current position in the array list
    //with their respective album numbers and their positions in that album

  //  Map<Integer, ArrayList<Integer>> resultAttrs = new HashMap<Integer, ArrayList<Integer>>();

    SparseArray<ArrayList<Integer>> resultAttrs = new SparseArray<ArrayList<Integer>>();


    boolean oneTag = false;
    boolean orFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        setContentView(R.layout.activity_tag_search);

        photosResult = new ArrayList<Photo>();

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        listView = (ListView) findViewById(R.id.listView);
        listView1 = (ListView) findViewById(R.id.listView1);

        switchAndOr = (Button) findViewById(R.id.switchAndOr);
        switchTags = (Button) findViewById(R.id.switchTags);

        tagKey1 = (Spinner) findViewById(R.id.spinnerTagKey1);
        tagKey2 = (Spinner) findViewById(R.id.spinnerTagKey2);

        tagVal1 = (EditText) findViewById(R.id.textTagValue1);
        tagVal2 = (EditText) findViewById(R.id.textTagValue2);

        buttonSearch = (Button) findViewById(R.id.buttonSearch);

        ArrayList<String> keyOpts = new ArrayList<String>();

        keyOpts.add("Location");
        keyOpts.add("Person");

        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, keyOpts);

        tagKey1.setAdapter(spinnerAdapter);
        tagKey2.setAdapter(spinnerAdapter);

        try {
            udata = new UserData(context);
        } catch (IOException e) {
            System.out.println("error instantiating user data");
            e.printStackTrace();
        }

        udata.loadData();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    public void search ( View v ) {

        if ( oneTag && tagVal1.getText().toString().equals("") )
            showErrorDialog("Please enter in a value for the tag.", "Empty field.");
        else if ( (! oneTag) && ( tagVal1.getText().toString().equals("") || tagVal2.getText().toString().equals("") ) )
            showErrorDialog("Please enter in a value for both tags.", "Empty fields.");
        else if ( oneTag )
            oneTagSearch();
        else if ( ! oneTag )
            twoTagSearch();

        System.out.println("Key 1: " + tagKey1.getSelectedItem().toString() + " Value 1: " + tagVal1.getText().toString());
        System.out.println("Key 2: " + tagKey2.getSelectedItem().toString() + " Value 2: " + tagVal2.getText().toString());

        System.out.println("Or flag: " + orFlag);
        System.out.println("One tag: " + oneTag);

        System.out.println(photosResult);

    }

    public boolean photoExists ( Photo p ) {

        for ( Photo x : photosResult )
            if ( p.equals(x) )
                return true;

        return false;
    }

    public void oneTagSearch () {

        photosResult.clear();
        resultAttrs.clear();

        listView1.setVisibility(View.VISIBLE);
        listView.setVisibility(View.INVISIBLE);

        int index = 0;

        Tag tag1 = new Tag(tagKey1.getSelectedItem().toString(), tagVal1.getText().toString());

        for ( int i = 0; i < udata.getAlbums().size(); i++ ) {

            Album a = udata.getAlbums().get(i);

            for ( int j = 0; j < a.getPhotos().size(); j++ ) {

                Photo p = a.getPhotos().get(j);

                boolean dec = ( tagExists(p.getTags(), tag1) );
                boolean aids = photoExists(p);
                System.out.println("Album : " + a.getName() + " Photo : " + p.getCaption() + " Tags : " + p.getTags() + " Decision? " +  dec + " ?? : " + aids);

                if ( (tagExists(p.getTags(), tag1)) && ( ! photoExists(p) ) ) {
                    ArrayList<Integer> attrs = new ArrayList<Integer>();
                    attrs.add(i);
                    attrs.add(j);
                    resultAttrs.put(index, attrs);
                    photosResult.add(p);
                    index++;
                }


            }

        }

        System.out.println("########################");
        System.out.println(photosResult);
        System.out.println("*************************");


        setupPhotoView();

    }

    public boolean tagExists( ArrayList<Tag> tags, Tag x ) {

        for ( Tag t : tags ) {

            boolean decA = ( x.equals(t) || (t.getTagType().equals(x.getTagType()) && t.getTagName().equals(x.getTagName())) );
            boolean decB = ( (t.getTagType().equals(x.getTagType())) && ( t.getTagName().indexOf(x.getTagName()) != -1 ) );

            System.out.println( "INDEX OF???? : " + (decA || decB));

            if ( decA || decB )
                return true;
        }
        return false;
    }

    public void twoTagSearch () {

        photosResult.clear();
        resultAttrs.clear();

        listView.setVisibility(View.VISIBLE);
        listView1.setVisibility(View.INVISIBLE);

        Tag tag1 = new Tag(tagKey1.getSelectedItem().toString(), tagVal1.getText().toString());
        Tag tag2 = new Tag(tagKey2.getSelectedItem().toString(), tagVal2.getText().toString());

        int index = 0;

        for ( int i = 0; i < udata.getAlbums().size(); i++ ) {

            Album a = udata.getAlbums().get(i);
            for ( int j = 0; j < a.getPhotos().size(); j++ ) {

                Photo p = a.getPhotos().get(j);
                boolean dec = ( tagExists(p.getTags(), tag1) ) || ( tagExists(p.getTags(), tag2));
                System.out.println("Album : " + a.getName() + " Photo : " + p.getCaption() + " Tags : " + p.getTags() + " Decision? " +  dec);

                if ( orFlag ) {
                    if ( ( (tagExists(p.getTags(), tag1)) || (tagExists(p.getTags(), tag2)) ) && ( ! photoExists(p) ) ) {
                        ArrayList<Integer> attrs = new ArrayList<>();
                        attrs.add(i);
                        attrs.add(j);
                        resultAttrs.put(index, attrs);
                        photosResult.add(p);
                        index++;
                    }
                } else {
                    if ( ( (tagExists(p.getTags(), tag1) ) && ( tagExists(p.getTags(), tag2)) ) && ( ! photoExists(p) )) {
                        ArrayList<Integer> attrs = new ArrayList<>();
                        attrs.add(i);
                        attrs.add(j);
                        resultAttrs.put(index, attrs);
                        photosResult.add(p);
                        index++;
                    }
                }

            }

        }

        System.out.println("########################");
        System.out.println(photosResult);
        System.out.println("*************************");


        setupPhotoView();
    }

    public void tagToggle ( View v ) {

        if ( oneTag ) {
            listView1.setVisibility(View.INVISIBLE);
            listView.setVisibility(View.VISIBLE);
        } else {
            listView.setVisibility(View.INVISIBLE);
            listView1.setVisibility(View.VISIBLE);
        }

        oneTag =! oneTag;

        if ( oneTag ) {
            switchTags.setText("1 TAG");
            switchAndOr.setVisibility(View.INVISIBLE);
            tagKey2.setVisibility(View.INVISIBLE);
            tagVal2.setVisibility(View.INVISIBLE);
        } else {
            switchTags.setText("2 TAGS");
            switchAndOr.setVisibility(View.VISIBLE);
            tagKey2.setVisibility(View.VISIBLE);
            tagVal2.setVisibility(View.VISIBLE);
        }



    }

    public void andOrToggler ( View v ) {

        orFlag =! orFlag;

        if ( orFlag )
            switchAndOr.setText("OR");
        else
            switchAndOr.setText("AND");

    }

    public void setupPhotoView () {

        if ( oneTag ) {
            AlbumViewAdapter adapter = new AlbumViewAdapter(this,R.layout.preview_item_layout, photosResult);

            listView1.setAdapter(adapter);

            adapter.notifyDataSetChanged();
            listView1.invalidateViews();

            listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                       Toast.makeText(getApplicationContext(), "You click on position: "+position + "  ~~~ " + id, Toast.LENGTH_SHORT).show();
                    viewPhotoAction(position);

                }
            });

        } else {
            AlbumViewAdapter adapter = new AlbumViewAdapter(this,R.layout.preview_item_layout, photosResult);

            listView.setAdapter(adapter);

            adapter.notifyDataSetChanged();
            listView.invalidateViews();

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                    Toast.makeText(getApplicationContext(), "You click on position: "+ position + "  ~~~ " + id, Toast.LENGTH_SHORT).show();

                    viewPhotoAction(position);

                }
            });

        }
    }

    public void reverseKeySearch () {

        ArrayList<Integer> deleted = new ArrayList<>();

        deleted.add(currentAlbumIndex);
        deleted.add(currentPhotoIndex);

        for ( int i = 0; i < resultAttrs.size(); i++ ) {

            if ( resultAttrs.get(i).equals(deleted) )
                photosResult.remove(i);

        }

    }

    @Override
    protected void onActivityResult( int requestCode, int result, Intent data ){

        if(requestCode == 1001) {
            // showToast(Integer.toString(result));
        }else if(requestCode == VIEW_PHOTO_CODE){
            System.out.println("Came back from photo view");
            Bundle bundle = data.getExtras();

            try {
                udata = new UserData(context);
            } catch (IOException e) {
                System.out.println("error instantiating user data");
                e.printStackTrace();
            }
            udata.loadData();

            boolean photoDeleted = bundle.getBoolean("photoDeleted");

            currentAlbumIndex = bundle.getInt("albumIndex");

            if ( photoDeleted ) {
                currentPhotoIndex = bundle.getInt("currentPhotoIndex");
                reverseKeySearch();

            }

            setupPhotoView();
        }else {
            if(data != null) {
                Uri uri = data.getData();
                System.out.println("URI!!!!!!:\t" + uri);
                Bitmap bitmap;
                try {
                    //     String path = Environment.getExternalStorageDirectory().getPath().concat(data.get)
                    bitmap = getBitmapFromUri(uri);
//                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                }catch (Exception e){
                    //        showToast("Unable to import local file.");
                    return;
                }

//                Photo p = new Photo(bitmap,uri);
                Photo p = new Photo(uri);
                p.setCaption(getFileNameFromURI(uri));
                System.out.println(data.toString());
                udata.getCurrentAlbum().getPhotos().add(p);

                System.out.println(udata.getCurrentAlbum().getPhotos());

                setupPhotoView();
            }
        }

    }

    public void viewPhotoAction( int position ) {

        System.out.println("we reached here to VIEW photo action");
        System.out.println("position to VIEW:\t" + position);
        Bundle b = new Bundle();
        b.putString("currAlbum", udata.getAlbums().get(resultAttrs.get(position).get(0)).getName());
        b.putInt("albumIndex",resultAttrs.get(position).get(0));
        b.putInt("currPhotoIndex", resultAttrs.get(position).get(1));
        b.putBoolean("isFromTagSearch",true);
        Intent intent = new Intent(this, ViewPhotoActivity.class);
        intent.putExtras(b);
        startActivityForResult(intent, VIEW_PHOTO_CODE);

    }


    public void showErrorDialog(final String message, final String title){
        AlertDialog aDialog = new AlertDialog.Builder(this).setMessage(message).setTitle(title)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        //Prevent to finish activity, if user clicks about.
//                        if (!title.equalsIgnoreCase("About") && !title.equalsIgnoreCase("Directory Error") && !title.equalsIgnoreCase("View")) {
//                            ((Activity) context).finish();
//                        }

                    }
                }).create();
        aDialog.show();
    }

    public Bitmap getBitmapFromUri(Uri uri) throws IOException {
//            if (checkPermissionREAD_EXTERNAL_STORAGE(context)) {
        System.out.println("bueno");
        ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return bitmap;
//            } else {
//                System.out.println("no bueno");
//                return null;
//            }


    }

    public String getFileNameFromURI(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public class AlbumViewAdapter extends ArrayAdapter<Photo> {
        public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
        // Store a member variable for the contacts
        private ArrayList<Photo> photos;
        private Context context;
        private int resource;
        private int lastPosition = -1;

        public class RegViewHolder {
            TextView caption;
            ImageView image;
            Button viewPhoto;
            Button deletePhoto;
        }

        // Pass in the contact array into the constructor
        public AlbumViewAdapter(Context context, int resource, ArrayList<Photo> photos) {
            super(context, resource, photos);
            this.photos = photos;
            this.context = context;
            this.resource = resource;

            setupImageLoader();
        }

        public Context getContext() {
            return context;
        }

        //            nameTextView = (TextView) itemView.findViewById(R.id.album_name);

        public View getView(final int position, View convertView, final ViewGroup parent) {

            setupImageLoader();

            String caption = getItem(position).getCaption();
            String path = getItem(position).getPath();

            final View result;

            RegViewHolder holder;

            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(resource, parent, false);
                holder = new RegViewHolder();
                holder.caption = (TextView) convertView.findViewById(R.id.textView1);
                holder.image = (ImageView) convertView.findViewById(R.id.image);

                result = convertView;

                convertView.setTag(holder);
            } else {
                holder = (RegViewHolder) convertView.getTag();
                result = convertView;
            }

            Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
            result.startAnimation(animation);
            lastPosition = position;

            holder.caption.setText(caption);

            ImageLoader imageLoader = ImageLoader.getInstance();

            int defaultImg = context.getResources().getIdentifier("@drawable/bet", null, context.getPackageName());

            //create display options
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .resetViewBeforeLoading(true)
                    .showImageForEmptyUri(defaultImg)
                    .showImageOnFail(defaultImg)
                    .showImageOnLoading(defaultImg).build();
            //cacheOnDisc(true).
            //download and display image from url

            if (path != null) {
           //     System.out.println("REGULAR DISPLAY");
                System.out.println(path);
                imageLoader.displayImage(path, holder.image, options);
            } else if (path == null) {
             //   System.out.println("SKETCH DISPLAY");
                Uri uri = photos.get(position).getUri();
                Bitmap bitmap;
                try {
                    bitmap = getBitmapFromUri(uri);
//                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                    holder.image.setImageBitmap(bitmap);
                } catch (Exception e) {
                    System.out.println("error on bitmap load duud");
                    e.printStackTrace();
                }
//            holder.image.setImageBitmap(photos.get(position).getBitmap());
            }


            return convertView;

        }

        private void setupImageLoader() {

            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .displayer(new FadeInBitmapDisplayer(300)).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    getContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .memoryCache(new WeakMemoryCache()).build();

            ImageLoader.getInstance().init(config);
        }

        public Bitmap getBitmapFromUri(Uri uri) throws IOException {
            ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
            FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
            Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
            parcelFileDescriptor.close();
            return bitmap;
        }
    }
}