package com.example.androidphotos;

import android.content.Context;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class UserData {


    private String storeFile = "albums.dat";
    private String numAlbumsFile = "numAlbums.txt";
    private ArrayList<Album> albums;
    private Album currentAlbum;
    private Photo currentPhoto;
    private int numAlbums=0;
    private Context context;


    public UserData (Context context) throws IOException {
        this.context=context;
        albums=new ArrayList<Album>();
        currentAlbum=null;
        currentPhoto=null;
    }


    /**
     * Saves the object data in a file
     */
    private void serialize() throws IOException {
        FileOutputStream fileOut;
        fileOut = context.openFileOutput(storeFile, Context.MODE_PRIVATE);
        int i=0;
        while(i< albums.size()) {
            Album temp= albums.get(i);
//			System.out.println("writing user: "+ temp.getUsername());
            try {
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(temp);
                System.out.println("saved: " + temp.getName());
            } catch (IOException e) {
                e.printStackTrace();
            }
            i++;
        }
        fileOut.close();

    }


    public void writeNumAlbums() throws IOException {
        String filename = numAlbumsFile;
        String fileContents = numAlbums+"";
        FileOutputStream outputStream;
        try {
            outputStream = context.openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(fileContents.getBytes());
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("wrote num albums (" + numAlbums +") to file");
    }

    /**
     * loads number of users from text file and stores it in numUsers
     * @throws IOException
     */
    public void loadNumAlbums() throws IOException {
        FileInputStream in = context.openFileInput(numAlbumsFile);
        InputStreamReader inputStreamReader = new InputStreamReader(in);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            sb.append(line);
        }
        String result = sb.toString();

        inputStreamReader.close();
        System.out.println("we got dis: " + result);
        numAlbums=Integer.parseInt(result);
    }

    /**
     * loads users back from serialized file
     */
    public void loadAlbums() throws IOException {
        Album temp = null;
        try {
            FileInputStream fileIn = context.openFileInput(storeFile);
//            FileInputStream fileIn = new FileInputStream(storeFile);
            if(fileIn.getChannel().size() == 0) {
                fileIn.close();
                return;
            }
            int i=0;
            while(i<numAlbums) {
                ObjectInputStream in = new ObjectInputStream(fileIn);
                temp = (Album) in.readObject();
                System.out.println("Added Album: " + temp.getName());
                albums.add(temp);
                i++;
            }
            System.out.println("loaded (" + albums.size() + ") albums to program");
            fileIn.close();
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("***FILE NOT FOUND, INITALIZING ALBUMS");
            albums = new ArrayList<Album>();
            return;
        } catch (ClassNotFoundException c) {
            System.out.println("Class not found");
            c.printStackTrace();
            return;
        }
    }

    public void saveData(){
        try {
            writeNumAlbums();
            serialize();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }
    }

    public void loadData(){
        try {
            loadNumAlbums();
            loadAlbums();
        } catch (java.io.IOException e){
            e.printStackTrace();
        }

    }

    public ArrayList<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(ArrayList<Album> albums) {
        this.albums = albums;
    }

    public Album getCurrentAlbum() {
        return currentAlbum;
    }

    public void setCurrentAlbum(Album currentAlbum) {
        this.currentAlbum = currentAlbum;
    }

    public Photo getCurrentPhoto() {
        return currentPhoto;
    }

    public void setCurrentPhoto(Photo currentPhoto) {
        this.currentPhoto = currentPhoto;
    }

    public int getNumAlbums() {
        return numAlbums;
    }

    public void setNumAlbums(int numAlbums) {
        this.numAlbums = numAlbums;
    }

    public String getNumAlbumsFile() {
        return numAlbumsFile;
    }

    public String getStoreFile() {
        return storeFile;
    }




}
