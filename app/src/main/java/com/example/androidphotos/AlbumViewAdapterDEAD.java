package com.example.androidphotos;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

public class AlbumViewAdapterDEAD extends ArrayAdapter<Photo> {

    // Store a member variable for the contacts
    private ArrayList<Photo> photos;
    private Context context;
    private int resource;
    private int lastPosition = -1;

    private static class RegViewHolder {
        TextView caption;
        ImageView image;
        Button viewPhoto;
        Button deletePhoto;
    }

    // Pass in the contact array into the constructor
    public AlbumViewAdapterDEAD(Context context, int resource, ArrayList<Photo> photos) {
        super(context, resource, photos);
        this.photos = photos;
        this.context=context;
        this.resource = resource;

        setupImageLoader();
    }

    public Context getContext(){
        return context;
    }

    //            nameTextView = (TextView) itemView.findViewById(R.id.album_name);

    public View getView(final int position, View convertView, final ViewGroup parent) {

        setupImageLoader();

        String caption = getItem(position).getCaption();
        String path = getItem(position).getPath();

        final View result;

        RegViewHolder holder;

        if ( convertView == null ) {
            LayoutInflater inflater = LayoutInflater.from(context);
            convertView = inflater.inflate(resource, parent, false);
            holder = new RegViewHolder();
            holder.caption = (TextView) convertView.findViewById(R.id.textView1);
            holder.image = (ImageView) convertView.findViewById(R.id.image);

            result = convertView;

            convertView.setTag(holder);
        } else {
            holder = (RegViewHolder) convertView.getTag();
            result = convertView;
        }

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
        result.startAnimation(animation);
        lastPosition = position;

        holder.caption.setText(caption);

        ImageLoader imageLoader = ImageLoader.getInstance();

        int defaultImg = context.getResources().getIdentifier("@drawable/bet",null, context.getPackageName());

        //create display options
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(defaultImg)
                .showImageOnFail(defaultImg)
                .showImageOnLoading(defaultImg).build();

        //download and display image from url

        if ( path != null ) {
            System.out.println("REGULAR DISPLAY");
            System.out.println(path);
            imageLoader.displayImage(path, holder.image, options);
        } else if ( path == null ) {
            System.out.println("SKETCH DISPLAY");
            Uri uri = photos.get(position).getUri();
            Bitmap bitmap;
            try {
                bitmap = getBitmapFromUri(uri);
//                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                holder.image.setImageBitmap(bitmap);
            }catch (Exception e){
               System.out.println("error on bitmap load duud");
               e.printStackTrace();
            }
//            holder.image.setImageBitmap(photos.get(position).getBitmap());
        }


        return convertView;

    }

    /**
     *
     * @param uri
     * @return
     * @throws IOException
     * using the photo uri, the image of the photo is able to be displayed.
     */
    public Bitmap getBitmapFromUri(Uri uri) throws IOException {

        ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return bitmap;
    }


    // Returns the total count of items in the list
    public int getItemCount() {
        return photos.size();
    }

    public void deleteItem(int position) {

        System.out.println("Item at position: " + position + " was attempted to get deleted");

        photos.remove(position);

        refreshList(photos);
        //   notifyItemRemoved(position);
    }

    private void setupImageLoader() {

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY)
                .displayer(new FadeInBitmapDisplayer(300)).build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getContext())
                .defaultDisplayImageOptions(defaultOptions)
                .memoryCache(new WeakMemoryCache()).build();

        ImageLoader.getInstance().init(config);
    }

    private void refreshList( ArrayList<Photo> list ) {
        this.photos.clear();
        this.photos.addAll(list);
        notifyDataSetChanged();
    }
}
