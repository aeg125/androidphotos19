package com.example.androidphotos;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import static com.example.androidphotos.MainActivity.IMAGE_SELECTED;


//https://www.youtube.com/watch?v=ZEEYYvVwJGY

public class ViewAlbumActivity extends AppCompatActivity {
    public static final int VIEW_PHOTO_CODE = 24;
    public static final String ALBUM_NAME = "currentAlbumName";
    public static final String ALBUM_INDEX = "albumIndex";
    private int albumIndex;
//    TextView currentAlbumName;
    private String result;

    String currentAlbumName;
    private Context context = this;


    LinearLayout linearLayout;
    ListView listView;
    Button deletePhoto;
    Button viewPhoto;

    public UserData udata;

//    ListView list = (ListView) findViewById(R.id.listView);


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_album);

        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);

        listView = (ListView) findViewById(R.id.listView);

        try {
            udata = new UserData(context);
        } catch (IOException e) {
            System.out.println("error instantiating user data");
            e.printStackTrace();
        }

        udata.loadData();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle bundle = getIntent().getExtras();
        albumIndex = bundle.getInt(ALBUM_INDEX);
        udata.setCurrentAlbum(udata.getAlbums().get(albumIndex));

        if (bundle!=null){
            currentAlbumName = bundle.getString(ALBUM_NAME);
        }

//        albumIndex = bundle.getInt("currAlbumIndex");
//        udata.setCurrentAlbum(udata.getAlbums().get(albumIndex));

        AlbumViewAdapter adapter = new AlbumViewAdapter(this,R.layout.preview_item_layout, udata.getCurrentAlbum().getPhotos());
        listView.setAdapter(adapter);

        setTitle("Album '" + currentAlbumName + "'");
//        toolbar.setTitleTextColor(Color.WHITE);

        adapter.notifyDataSetChanged();
        listView.invalidateViews();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(getApplicationContext(), "You click on position: "+ position + "  ~~~ " + id, Toast.LENGTH_SHORT).show();

                viewPhotoAction(position);


            }
        });

    }

    public void refreshAlbum() {
        setTitle("Album '" + currentAlbumName + "'");
        udata.saveData();
        setTitle("Album '" + udata.getCurrentAlbum().getName() + "'");

        setContentView(R.layout.activity_view_album);

        listView = (ListView) findViewById(R.id.listView);

        AlbumViewAdapter adapter = new AlbumViewAdapter(this,R.layout.preview_item_layout, udata.getCurrentAlbum().getPhotos());
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

//                Toast.makeText(getApplicationContext(), "You click on position: "+ position + "  ~~~ " + id, Toast.LENGTH_SHORT).show();

                viewPhotoAction(position);


            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }


    /**
     *
     * @param uri
     * @return
     * @throws IOException
     * using the photo uri, the image of the photo is able to be displayed.
     */
    public Bitmap getBitmapFromUri(Uri uri) throws IOException {
        ParcelFileDescriptor parcelFileDescriptor = getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return bitmap;
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu_album,menu);
        return true;
//        return super.onCreateOptionsMenu(menu);

    }


    public void viewPhotoAction( int position ) {
        System.out.println("we reached here to VIEW photo action");
        System.out.println("position to VIEW:\t"+ position);
        Bundle b = new Bundle();
        b.putString("currAlbum",udata.getCurrentAlbum().getName());
        b.putInt(ALBUM_INDEX,albumIndex);
        b.putInt("currPhotoIndex",position);
        Intent intent = new Intent(this, ViewPhotoActivity.class);
        intent.putExtras(b);
        startActivityForResult(intent,VIEW_PHOTO_CODE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            System.out.println("**************leaving");
            String name = udata.getCurrentAlbum().getName();
            // make Bundle
            Bundle bundle = new Bundle();
            bundle.putString(ALBUM_NAME,name);
            bundle.putInt(ALBUM_INDEX, albumIndex);
            Intent intent = new Intent();
            intent.putExtras(bundle);
            setResult(RESULT_OK,intent);
            udata.setCurrentAlbum(null);
            udata.saveData();
            finish();
            return true;
        } else if (item.getItemId()==R.id.action_add_photo) {
            System.out.println("addPhoto was clicked");
            importPhotos();
            return true;
        } else if ( item.getItemId() == R.id.action_edit_album_name ) {
            System.out.println("Rename album clicked");
            renameAlbum();
            return true;
        } else if (item.getItemId() == R.id.action_slideshow){
            System.out.println("Slideshow");
            goToSlideshow();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void goToSlideshow(){
        Bundle b = new Bundle();
        b.putInt(ALBUM_INDEX,albumIndex);
        Intent intent = new Intent(this, SlideshowActivity.class);
        intent.putExtras(b);
        startActivityForResult(intent,VIEW_PHOTO_CODE);
    }

    public boolean albumExists ( String name ) {

        for ( int i = 0; i < udata.getAlbums().size(); i++ )
            if ( 0 == name.compareTo(udata.getAlbums().get(i).getName()) )
                return true;

        return false;
    }


    public void renameAlbum() {

        LayoutInflater li = LayoutInflater.from(this);
        View promptView = li.inflate(R.layout.prompt_rename_album, null);

        AlertDialog.Builder adb = new AlertDialog.Builder(this);

        adb.setView(promptView);

        final EditText userInput = (EditText) promptView.findViewById(R.id.albumNameInput);

        adb.setCancelable(false)
                .setPositiveButton("Ok",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                result = userInput.getText().toString();
                                result = result.replace("\n", "").replace("\r", "");
                                System.out.println("INPUT: " + result);

                                if ( ! albumExists(result) && ! (0 == result.compareTo(""))  ) {
                                    udata.getCurrentAlbum().setName(result);
                                    System.out.println(udata.getCurrentAlbum());

                                    refreshAlbum();
                                } else {
                                    System.out.println("THIS ALBUM NAME EXISTS");
                                    showErrorDialog("Please choose a different album name.",
                                            "This album name already exists.");
                                }

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.cancel();
                            }
                        });

        AlertDialog ad = adb.create();

        ad.show();

    }

    public void showErrorDialog(final String message, final String title){
        AlertDialog aDialog = new AlertDialog.Builder(this).setMessage(message).setTitle(title)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        //Prevent to finish activity, if user clicks about.
//                        if (!title.equalsIgnoreCase("About") && !title.equalsIgnoreCase("Directory Error") && !title.equalsIgnoreCase("View")) {
//                            ((Activity) context).finish();
//                        }

                    }
                }).create();
        aDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int result, Intent data){

        if(requestCode == 1001) {
            // showToast(Integer.toString(result));
        }else if(requestCode == VIEW_PHOTO_CODE){
            System.out.println("Came back from photo view");
            Bundle bundle = data.getExtras();

            try {
                udata = new UserData(context);
            } catch (IOException e) {
                System.out.println("error instantiating user data");
                e.printStackTrace();
            }
            udata.loadData();
            albumIndex = bundle.getInt(ALBUM_INDEX);

            System.out.println("******Album index returned is:\t" + albumIndex);
            udata.setCurrentAlbum(udata.getAlbums().get(albumIndex));
            refreshAlbum();
        }else {
            if(data != null) {
                Uri uri = data.getData();
                System.out.println("URI!!!!!!:\t" + uri);
                Bitmap bitmap;
                try {
               //     String path = Environment.getExternalStorageDirectory().getPath().concat(data.get)
                    bitmap = getBitmapFromUri(uri);
//                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                }catch (Exception e){
            //        showToast("Unable to import local file.");
                    return;
                }

//                Photo p = new Photo(bitmap,uri);
                Photo p = new Photo(uri);
                p.setCaption(getFileNameFromURI(uri));
                System.out.println(data.toString());
                udata.getCurrentAlbum().getPhotos().add(p);

                System.out.println(udata.getCurrentAlbum().getPhotos());

                refreshAlbum();
            }
        }

    }


    public String getFileNameFromURI(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }


    public void importPhotos() {
        /*
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture to Import"), SELECT_IMAGE);
    */

        Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
        getIntent.setType("image/*");


//        // Filesystem.
//        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        Intent pickIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickIntent.setType("image/*");


        Intent chooserIntent = Intent.createChooser(getIntent, "Select Image to Import");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[] {pickIntent});

        System.out.println("BEFORE " + IMAGE_SELECTED);

        startActivityForResult(chooserIntent, IMAGE_SELECTED);

        System.out.println("AFTER " + IMAGE_SELECTED);
        udata.saveData();

   }


    public class AlbumViewAdapter extends ArrayAdapter<Photo> {
        public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
        // Store a member variable for the contacts
        private ArrayList<Photo> photos;
        private Context context;
        private int resource;
        private int lastPosition = -1;

        public class RegViewHolder {
            TextView caption;
            ImageView image;
            Button viewPhoto;
            Button deletePhoto;
        }

        // Pass in the contact array into the constructor
        public AlbumViewAdapter(Context context, int resource, ArrayList<Photo> photos) {
            super(context, resource, photos);
            this.photos = photos;
            this.context=context;
            this.resource = resource;

            setupImageLoader();
        }

        public Context getContext(){
            return context;
        }

        //            nameTextView = (TextView) itemView.findViewById(R.id.album_name);

        public View getView(final int position, View convertView, final ViewGroup parent) {

            setupImageLoader();

            String caption = getItem(position).getCaption();
            String path = getItem(position).getPath();

            final View result;

            RegViewHolder holder;

            if ( convertView == null ) {
                LayoutInflater inflater = LayoutInflater.from(context);
                convertView = inflater.inflate(resource, parent, false);
                holder = new RegViewHolder();
                holder.caption = (TextView) convertView.findViewById(R.id.textView1);
                holder.image = (ImageView) convertView.findViewById(R.id.image);

                result = convertView;

                convertView.setTag(holder);
            } else {
                holder = (RegViewHolder) convertView.getTag();
                result = convertView;
            }

            Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.load_down_anim : R.anim.load_up_anim);
            result.startAnimation(animation);
            lastPosition = position;

            holder.caption.setText(caption);

            ImageLoader imageLoader = ImageLoader.getInstance();

            int defaultImg = context.getResources().getIdentifier("@drawable/bet",null, context.getPackageName());

            //create display options
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .resetViewBeforeLoading(true)
                    .showImageForEmptyUri(defaultImg)
                    .showImageOnFail(defaultImg)
                    .showImageOnLoading(defaultImg).build();
            //cacheOnDisc(true).
            //download and display image from url

            if ( path != null ) {
                System.out.println("REGULAR DISPLAY");
                System.out.println(path);
                imageLoader.displayImage(path, holder.image, options);
            } else if ( path == null ) {
                System.out.println("SKETCH DISPLAY");
                Uri uri = photos.get(position).getUri();
                Bitmap bitmap;
                try {
                    bitmap = getBitmapFromUri(uri);
//                bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
                    holder.image.setImageBitmap(bitmap);
                }catch (Exception e){
                    System.out.println("error on bitmap load duud");
                    e.printStackTrace();
                }
//            holder.image.setImageBitmap(photos.get(position).getBitmap());
            }


            return convertView;

        }

        /**
         *
         * @param uri
         * @return
         * @throws IOException
         * using the photo uri, the image of the photo is able to be displayed.
         */
        public Bitmap getBitmapFromUri(Uri uri) throws IOException {
//            if (checkPermissionREAD_EXTERNAL_STORAGE(context)) {
                System.out.println("bueno");
                ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
                FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                parcelFileDescriptor.close();
                return bitmap;
//            } else {
//                System.out.println("no bueno");
//                return null;
//            }


        }

        public boolean checkPermissionREAD_EXTERNAL_STORAGE(
                final Context context) {
            int currentAPIVersion = Build.VERSION.SDK_INT;
            if (currentAPIVersion >= android.os.Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(context,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            (Activity) context,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                        System.out.println("no permission?");

                    } else {
                        ActivityCompat
                                .requestPermissions(
                                        (Activity) context,
                                        new String[] { Manifest.permission.READ_EXTERNAL_STORAGE },
                                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                    return false;
                } else {
                    return true;
                }

            } else {
                return true;
            }
        }

        public void deleteItem(int position) {

            System.out.println("Item at position: " + position + " was attempted to get deleted");

            photos.remove(position);

            refreshList(photos);
            //   notifyItemRemoved(position);
        }

        private void setupImageLoader() {

            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .displayer(new FadeInBitmapDisplayer(300)).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    getContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .memoryCache(new WeakMemoryCache()).build();

            ImageLoader.getInstance().init(config);
        }

        private void refreshList(ArrayList<Photo> list ) {
            this.photos.clear();
            this.photos.addAll(list);
            notifyDataSetChanged();
        }
    }









}
