package com.example.androidphotos;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

import static com.example.androidphotos.ViewPhotoActivity.ALBUM_INDEX;

public class SlideshowActivity extends AppCompatActivity {

    private UserData udata;
    private int currentAlbumIndex;
    private Context context;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        context = this;
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_slideshow);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            udata = new UserData(this);
        } catch (IOException e) {
            System.out.println("error instantiating user data");
            e.printStackTrace();
        }
        udata.loadData();

        Bundle b = getIntent().getExtras();
        currentAlbumIndex = b.getInt(ALBUM_INDEX);


        udata.setCurrentAlbum(udata.getAlbums().get(currentAlbumIndex));
        setTitle("Slideshow of Album '" + udata.getCurrentAlbum()+"'");
        System.out.println("current album:\t" + udata.getCurrentAlbum());
        System.out.println(udata.getCurrentAlbum().getPhotos());

        ViewPager viewPager = findViewById(R.id.photoViewPager);
        ImageAdapter adapter = new ImageAdapter(context, udata.getCurrentAlbum().getPhotos());
        System.out.println(adapter.getCount());
        viewPager.setAdapter(adapter);

    }




    /**
     *
     * @param uri
     * @return
     * @throws IOException
     * using the photo uri, the image of the photo is able to be displayed.
     */
    public Bitmap getBitmapFromUri(Uri uri) throws IOException {
        System.out.println("bueno");
        ParcelFileDescriptor parcelFileDescriptor = this.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return bitmap;

    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            System.out.println("**************leaving PHOTO");
            // make Bundle
            Bundle bundle = new Bundle();
            bundle.putInt(ALBUM_INDEX, currentAlbumIndex);
            Intent intent = new Intent();
            intent.putExtras(bundle);
            setResult(RESULT_OK,intent);
            udata.setCurrentAlbum(null);
            udata.saveData();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public class ImageAdapter extends PagerAdapter {
        private Context context;
        private ArrayList<Photo> photos;

        public ImageAdapter(Context context, ArrayList<Photo> photos) {
            this.context = context;
            this.photos = photos;
        }

        @Override
        public int getCount() {
            return photos.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
            return view==object;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            container.removeView((ImageView)object);
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            System.out.println("current Position: " + position);
            ImageView imageView = new ImageView(context);
//            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Bitmap bitmap;

            try {
                bitmap = getBitmapFromUri(photos.get(position).getUri());
                imageView.setImageBitmap(bitmap);
            }catch (Exception e){
                System.out.println("error on bitmap load duudleeee");
                e.printStackTrace();
            }
            container.addView(imageView,0);
            return imageView;
        }
    }



}
