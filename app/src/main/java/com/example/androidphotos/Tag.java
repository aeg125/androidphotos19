package com.example.androidphotos;

import java.util.Comparator;


public class Tag implements java.io.Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String tagType;
    private String tagName;

    @Override
    public String toString() {
        return tagType +"=" +tagName;
    }

    public Tag(String tagType, String tagName) {
        this.tagType = tagType;
        this.tagName = tagName;
    }

    public String getTagType() {
        return tagType;
    }
    public void setTagType(String tagType) {
        this.tagType = tagType;
    }
    public String getTagName() {
        return tagName;
    }
    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Tag))
            return false;
        Tag other = (Tag) obj;
        if (tagName == null) {
            if (other.tagName != null)
                return false;
        } else if (!tagName.equals(other.tagName))
            return false;
        if (tagType == null) {
            if (other.tagType != null)
                return false;
        } else if (!tagType.equals(other.tagType))
            return false;
        return true;
    }

    public static Comparator<Tag> tagNameCompare = new Comparator<Tag>() {
        @Override
        public int compare(Tag t1, Tag t2) throws NullPointerException {
            return t1.getTagName().compareTo(t2.getTagName());
        }
    };


}
