package com.example.androidphotos;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;


public class Album implements java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String name;
    private ArrayList<Photo> photos;

    public Album(String name, ArrayList<Photo> photos) {
        this.name = name;
        this.photos = photos;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public ArrayList<Photo> getPhotos() {
        return photos;
    }

    public boolean addPhoto(Photo photo) {
        if (photos.contains(photo)) {
            return false;
        }
        photos.add(photo);
        return true;
    }

    public boolean removePhoto(Photo photo) {
        if(photos.contains(photo)) {
            photos.remove(photo);
            return true;
        }
        return false;
    }


    public String toString() {
        return name;
    }

    public static Comparator<Album> AlbumNameComparator = new Comparator<Album>() {
        public int compare(Album album1, Album album2) {
            String albumName1 = album1.getName().toUpperCase();
            String albumName2 = album2.getName().toUpperCase();

            //ascending order
            return albumName1.compareTo(albumName2);

        }
    };



}








