package com.example.androidphotos;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
public class AlbumsAdapter extends RecyclerView.Adapter<AlbumsAdapter.ViewHolder> {
    public interface OnItemClickListener {
        void onItemClick(Album item, int position);
    }

    // Store a member variable for the contacts
    private ArrayList<Album> albums;
    private Context context;
    private final OnItemClickListener listener;
    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    public class ViewHolder extends RecyclerView.ViewHolder {
        // Your holder should contain a member variable
        // for any view that will be set as you render a row
        public TextView nameTextView;

        // We also create a constructor that accepts the entire item row
        // and does the view lookups to find each subview
        public ViewHolder(View itemView) {
            // Stores the itemView in a public final member variable that can be used
            // to access the context from any ViewHolder instance.
            super(itemView);

            nameTextView = (TextView) itemView.findViewById(R.id.album_name);
        }

        public void bind(final Album item, final int position, final OnItemClickListener listener) {
//            name.setText(item.name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item,position);
                }
            });
        }

    }

    // Pass in the contact array into the constructor
    public AlbumsAdapter(ArrayList<Album> albums, Context c, OnItemClickListener listener) {
        this.albums = albums;
        this.context=c;
        this.listener=listener;
    }

    public Context getContext(){
        return context;
    }

    // Usually involves inflating a layout from XML and returning the holder
    @Override
    public AlbumsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View contactView = inflater.inflate(R.layout.album_list_item, parent, false);

        // Return a new holder instance
        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    // Involves populating data into the item through holder
    @Override
    public void onBindViewHolder(AlbumsAdapter.ViewHolder viewHolder, int position) {
        // Get the data model based on position
        Album album = albums.get(position);
        viewHolder.bind(albums.get(position), position, listener);
        // Set item views based on your views and data model
        TextView textView = viewHolder.nameTextView;
        textView.setText(album.getName());
    }

    // Returns the total count of items in the list
    @Override
    public int getItemCount() {
        return albums.size();
    }

    public void deleteItem(int position) {
        albums.remove(position);
        notifyItemRemoved(position);
    }
}