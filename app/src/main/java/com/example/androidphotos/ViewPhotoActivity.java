package com.example.androidphotos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.FileDescriptor;
import java.io.IOException;
import java.util.ArrayList;

/*
For MENU BAR:
- slideshow button/Swipey stuff instead?
- add tag button
- move photo button
 */
public class ViewPhotoActivity extends AppCompatActivity {

    String result;

    public static final String ALBUM_INDEX = "albumIndex";
    private int currentAlbumIndex;
    private int currentPhotoIndex;
    String currentAlbumName;
    private Context context = this;
    private Photo currentPhoto;

    boolean photoDeleted = false;

    ArrayList<Tag> tags;
    ImageView photoImageView;
    LinearLayout linearLayout;
    ListView tagsListView;

    boolean isFromTagSearch;
    public UserData udata;
    Button previousPhotoButton;
    Button nextPhotoButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_photo);

        linearLayout = findViewById(R.id.linearLayout);
        photoImageView = findViewById(R.id.photoImageView);
        tagsListView = findViewById(R.id.tagsListView);
        nextPhotoButton = findViewById(R.id.nextPhoto);
        previousPhotoButton = findViewById(R.id.previousPhoto);

        try {
            udata = new UserData(context);
        } catch (IOException e) {
            System.out.println("error instantiating user data");
            e.printStackTrace();
        }

        udata.loadData();

        Bundle b = getIntent().getExtras();
        currentAlbumName = b.getString("currAlbum");
        currentAlbumIndex = b.getInt(ALBUM_INDEX);
        currentPhotoIndex = b.getInt("currPhotoIndex");
        isFromTagSearch=b.getBoolean("isFromTagSearch");

        if(isFromTagSearch){
            System.out.println("******WE CAME FROM SEARCH");
            previousPhotoButton.setVisibility(View.INVISIBLE);
            nextPhotoButton.setVisibility(View.INVISIBLE);
        }
        udata.setCurrentAlbum(udata.getAlbums().get(currentAlbumIndex));

        System.out.println("current photo index:\t" + currentPhotoIndex);
        currentPhoto= udata.getCurrentAlbum().getPhotos().get(currentPhotoIndex);

        System.out.println("!!!~~~~~~~~~~~~~");
        System.out.println(currentPhoto);

        System.out.println(currentPhoto.emptyTags());

        udata.setCurrentPhoto(currentPhoto);

//
//        ViewPager viewPager = findViewById(R.id.photoViewPager);
//        ImageAdapter adapter = new ImageAdapter(this, udata.getCurrentAlbum().getPhotos(), currentPhotoIndex);
//        viewPager.setAdapter(adapter);

        setPhoto(); //sets the imageview to the photo

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(currentPhoto.getCaption());

        tagsListView.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, currentPhoto.getTags()));
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu_photo,menu);
        return true;
    }



    public void prevPhoto(View v){
        System.out.println("previous photo");
        if((currentPhotoIndex-1)<0) {
            currentPhotoIndex=udata.getCurrentAlbum().getPhotos().size();
        }
        currentPhotoIndex--;
        currentPhoto = udata.getCurrentAlbum().getPhotos().get(currentPhotoIndex);
        udata.setCurrentPhoto(currentPhoto);
        setPhoto();
        setTitle(currentPhoto.getCaption());
        tagsListView.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, currentPhoto.getTags()));

    }


    public void nextPhoto(View v){
        System.out.println("next photo");
        if((currentPhotoIndex+1)>=udata.getCurrentAlbum().getPhotos().size()) {
            currentPhotoIndex=-1;
        }
        currentPhotoIndex++;
        currentPhoto = udata.getCurrentAlbum().getPhotos().get(currentPhotoIndex);
        udata.setCurrentPhoto(currentPhoto);
        setPhoto();
        setTitle(currentPhoto.getCaption());
        tagsListView.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, currentPhoto.getTags()));
    }


    public void deletePhotoAction() {

        System.out.println("we reached here to DELETE photo action");

        Album a = udata.getCurrentAlbum();
        Photo p = udata.getCurrentPhoto();

        try {
            System.out.println("Number of photos" + udata.getCurrentAlbum().getPhotos().size());
            if (a.getPhotos().size() > 0) {
                System.out.println("PHOTOS:");
                System.out.println(udata.getCurrentAlbum().getPhotos());
            }
            a.removePhoto(p);
            udata.saveData();

            photoDeleted = true;

            exitActivity();
            //      setTitle("Album '" + currentAlbumName + "'");

        } catch (Exception e) {
            //      showErrorDialog(e.toString(), "Photo didn't get removed");
            e.printStackTrace();

        }

    }

    public void exitActivity(){
        System.out.println("**************leaving PHOTO");
        // make Bundle
        Bundle bundle = new Bundle();
        bundle.putInt(ALBUM_INDEX, currentAlbumIndex);

        bundle.putBoolean("photoDeleted", photoDeleted);
        bundle.putInt("currentPhotoIndex", currentPhotoIndex );

        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK,intent);
        udata.setCurrentAlbum(null);
        udata.saveData();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
           exitActivity();
            return true;
        } else if (item.getItemId()==R.id.actionAddTag) {
            System.out.println("* * * * add tag button clicked");
            addTagAction();

        } else if (item.getItemId() == R.id.actionMoveToAlbum) {
            System.out.println("* * * * move to album button clicked");
            moveToAlbumAction();
        } else if (item.getItemId() == R.id.actionDeletePhoto) {
            System.out.println("* * * * delete photo button clicked");
            deletePhotoAction();

        }

        return super.onOptionsItemSelected(item);
    }

    public void addTagAction() {

        // setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add a tag or remove a tag?");

// add a list
        String[] options = {"Add Tag", "Remove Tag"};
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: // Add tag
                        System.out.println("You clicked add tag");
                        addTagChoices();
                        break;
                    case 1:
                        System.out.println("You clicked remove tag");

                        if ( udata.getCurrentPhoto().getTags().isEmpty() ) {
                            showErrorDialog("There are no tags for this photo.", "No tags");
                            break;
                        }
                        removeTagChoices();
                        break;
                }
            }
        });

// create and show the alert dialog
        AlertDialog dialog = builder.create();
        dialog.show();


    }

    public boolean existingTag(String name) {

        if ( currentPhoto.emptyTags() )
            return false;

        for ( Tag x : currentPhoto.getTags() )
            if ( 0 == x.getTagName().compareTo(name) )
                return true;

        return false;
    }

    public boolean containsLocationTag(){
        if(currentPhoto.getTags().size()==0){
            return false;
        }
        for(Tag t : currentPhoto.getTags()){
            if(t.getTagType().equals("Location")){
                System.out.println(t.getTagType());
                return true;
            }
        }
        return false;
    }
    public void tagDetail( final int choice ) {

        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompt_tag_name, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.tagChoiceInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                result = userInput.getText().toString();
                                result = result.replace("\n", "").replace("\r", "");
                                System.out.println("the user entered: " + result);
                                if( ! existingTag(result) ){

                                    switch (choice) {
                                        case 0:
                                            if(containsLocationTag()){
                                                System.out.println("location tag already exists");
                                                showErrorDialog("A location tag already exists. Limit 1 location tag per photo", "Location tag already exists");
                                            } else { //location tag doesnt exist yet
                                                currentPhoto.addTag("Location", result);
                                            }
                                            break;
                                        case 1:
                                            currentPhoto.addTag("Person", result);
                                            break;
                                    }

                                    udata.saveData();
                                    refreshTagsListView();
                                    System.out.println("WE ADDED A TAG???");
                                    System.out.println(currentPhoto.getTags());

                                } else {
                                    System.out.println("Tag already exists!");
                                    showErrorDialog("Error, the tag name you entered already exists either as a location or a person.",
                                            "Tag name already exists");
                                }

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

    }



    public void addTagChoices() {

        AlertDialog.Builder addBuilder = new AlertDialog.Builder(context);
        addBuilder.setTitle("Choose the type of tag.");

        String[] addOptions = {"Location", "Person"};
        addBuilder.setItems(addOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int choice) {
                tagDetail(choice);
            }
        });

        AlertDialog dialog = addBuilder.create();
        dialog.show();


    }

    public void removeTagChoices() {
        AlertDialog.Builder addBuilder = new AlertDialog.Builder(context);
        addBuilder.setTitle("Choose the tag you want to remove:");

        ArrayList<Tag> tags = udata.getCurrentPhoto().getTags();

        String[] removeOptions = new String[tags.size()];


        for ( int i = 0; i < tags.size(); i++ )
            removeOptions[i] = (tags.get(i).getTagType().concat(": ").concat(tags.get(i).getTagName()));



        addBuilder.setItems(removeOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int choice) {
                udata.getCurrentPhoto().getTags().remove(choice);
                refreshTagsListView();
                udata.saveData();

            }
        });

        AlertDialog dialog = addBuilder.create();
        dialog.show();

    }


    public void moveToAlbumAction() {
        AlertDialog.Builder moveToAlbumBuilder = new AlertDialog.Builder(context);
        moveToAlbumBuilder.setTitle("Choose the album you want to move this to:");

        ArrayList<String> albumNames = new ArrayList<String>();

       final ArrayList<Album> albums = udata.getAlbums();

        String[] moveOptions = new String[albums.size()];


        for ( int i = 0; i < albums.size(); i++ )
            moveOptions[i] = albums.get(i).getName();


        moveToAlbumBuilder.setItems(moveOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int choice) {
                Photo p = currentPhoto;
                System.out.println("*********photo to move: " + currentPhoto);
                udata.getAlbums().get(choice).getPhotos().add(p);
                System.out.println("*********"+udata.getAlbums().get(choice).getPhotos());
                udata.getAlbums().get(currentAlbumIndex).getPhotos().remove(currentPhoto);
                udata.saveData();
                exitActivity();

            }
        });

        AlertDialog dialog = moveToAlbumBuilder.create();
        dialog.show();


    }

    public void setPhoto(){
        ImageLoader imageLoader = ImageLoader.getInstance();
        Uri uri = currentPhoto.getUri();
        Bitmap bitmap;
        try {
            bitmap = getBitmapFromUri(uri);
            photoImageView.setImageBitmap(bitmap);
        }catch (Exception e){
            System.out.println("error on bitmap load duud");
            e.printStackTrace();
        }
        tagsListView.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, currentPhoto.getTags()));
    }

    public void refreshTagsListView(){
        tagsListView.setAdapter(new ArrayAdapter<Tag>(this, R.layout.tag, currentPhoto.getTags()));
    }
    public void showErrorDialog(final String message, final String title){
        AlertDialog aDialog = new AlertDialog.Builder(context).setMessage(message).setTitle(title)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        //Prevent to finish activity, if user clicks about.
//                        if (!title.equalsIgnoreCase("About") && !title.equalsIgnoreCase("Directory Error") && !title.equalsIgnoreCase("View")) {
//                            ((Activity) context).finish();
//                        }

                    }
                }).create();
        aDialog.show();
    }

    /**
     *
     * @param uri
     * @return
     * @throws IOException
     * using the photo uri, the image of the photo is able to be displayed.
     */
    public Bitmap getBitmapFromUri(Uri uri) throws IOException {
        System.out.println("bueno");
        ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
        FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
        Bitmap bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
        parcelFileDescriptor.close();
        return bitmap;

    }










}
