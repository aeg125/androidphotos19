package com.example.androidphotos;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


public class Photo implements java.io.Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private String path;
    private String uri;
    private String caption;
    private long date;
    private ArrayList<Tag> tags;
    private byte[] bitmapBytes;

    public Photo(Bitmap bits, Uri uri) {
        this.bitmapBytes = bitsToBytes(bits);
        this.uri = uri.toString();
    }

    public Photo(Uri uri) {
        this.uri = uri.toString();
        this.tags = new ArrayList<Tag>();
    }

    public Photo(String path, String caption, long date, ArrayList<Tag> tags) {
        this.path = path;
        this.caption = caption;
        this.date = date;
        this.tags = tags;
    }
    public Uri getUri(){
        Uri myUri = Uri.parse(uri);
        return myUri;
    }
    public String getPath() {
        return path;
    }
    public void setPath(String path) {
        this.path = path;
    }
    public String getCaption() {
        return caption;
    }
    public void setCaption(String caption) {
        this.caption = caption;
    }
    public String getDateString() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/ yyyy HH:mm:ss");
        return sdf.format(date);
    }

    public boolean emptyTags() {
        if ( tags == null || tags.isEmpty() )
            return false;
        else
            return true;
    }

    public Bitmap getBitmap(){
        return BitmapFactory.decodeByteArray(this.bitmapBytes, 0, this.bitmapBytes.length);
    }

    public long getDateLong() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }
    public ArrayList<Tag> getTags() {

        if ( tags == null )
            tags = new ArrayList<Tag>();

        return tags;
    }
    public boolean addTag(String tagType, String tagValue) {

        if ( tags == null ) {
            tags = new ArrayList<Tag>();
        }
        Tag t = new Tag(tagType,tagValue);
        if(tags.contains(t)) {
            return false;
        }
        this.tags.add(t);
        return true;
    }

    public boolean removeTag(String tagType, String tagValue) {
        Tag toRemove = new Tag(tagType,tagValue);
        for(Tag t : tags) {
            if(toRemove.equals(t)) {
                tags.remove(t);
                return true; //tag removed
            }
        }
        return false; //tag not removed because not found
    }

    public String toString() {
        return "path: " +  path +  "\ncaption: " + caption +  "\ndate: " + this.getDateString() +  "\ntags: " + tags;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Photo))
            return false;
        Photo other = (Photo) obj;

        if ( path == null ) {
            if ( uri.equals(((Photo) obj).uri) )
                if ( caption.equals(((Photo) obj).caption) )
                     return true;
            return false;
        } else if ( ! path.equals(other.path) ) {
            return false;
        } else if (! caption.equals(other.caption))
            return false;
        return true;
    }

    private byte[] bitsToBytes(Bitmap bits){
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bits.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        bits.recycle();

        return byteArray;
    }

}

