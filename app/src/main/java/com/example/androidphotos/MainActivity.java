package com.example.androidphotos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import com.google.gson.Gson;


public class MainActivity extends AppCompatActivity {

//    private ArrayList<Album> albums;

//    private String storeFile = "albums.dat";
//    private String numAlbumsFile = "numAlbums.txt";
//    public static int numAlbums = 0;

    private ListView albumListView;
    private String result;
    protected final Context context = this;
    private RecyclerView albumsRV;

    public static final int VIEW_ALBUM_CODE=1;
    public static final int SEARCH_BY_TAG_CODE = 10;
    public static final int IMAGE_SELECTED = 1;

    private UserData udata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {
            udata = new UserData(context);
        } catch (IOException e) {
            System.out.println("error instantiating user data");
            e.printStackTrace();
        }

//        albums = new ArrayList<Album>();
        udata.loadData();
        albumsRV = (RecyclerView) findViewById(R.id.albums_list_rv);
        // attaching the touch helper to recycler view
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(albumsRV);
        setAlbumsRecyclerView();
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                addAlbum();
                return true;
//            case android.R.id.home:
//                System.out.println("***weback");
//                return true;

            case R.id.action_search_by_tag:
                searchByTag();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void searchByTag() {
        Intent intent = new Intent(this, SearchByTagActivity.class);
        startActivityForResult(intent, SEARCH_BY_TAG_CODE);
    }

    //adds album to arraylist
    public void addAlbum(){

//        EditText result = (EditText) findViewById(R.id.editTextResult);

        System.out.println("will add album");
//        Intent i = new Intent(getApplicationContext(), AddAlbumPopActivity.class);
        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompt_add_album, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.albumNameInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                result = userInput.getText().toString();
                                result = result.replace("\n", "").replace("\r", "");
                                System.out.println("the user entered: " + result);
                                if(!existingAlbum(result)){
                                    ArrayList<Photo> photos = new ArrayList<Photo>();
                                    Album a = new Album(result, photos);
                                    udata.getAlbums().add(a);
                                    udata.setNumAlbums(udata.getAlbums().size());
                                    udata.saveData();
                                } else {
                                    System.out.println("album already exists!");
                                    showErrorDialog("Error, the album name you entered already exists",
                                            "Album name already exists");
                                }

                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        // show it
        alertDialog.show();

        setAlbumsRecyclerView();

    }



    public void showErrorDialog(final String message, final String title){
        AlertDialog aDialog = new AlertDialog.Builder(context).setMessage(message).setTitle(title)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog,
                                        final int which) {
                        //Prevent to finish activity, if user clicks about.
//                        if (!title.equalsIgnoreCase("About") && !title.equalsIgnoreCase("Directory Error") && !title.equalsIgnoreCase("View")) {
//                            ((Activity) context).finish();
//                        }

                    }
                }).create();
        aDialog.show();
    }

    /**
     * checks if an album already exists for the user
     * @param s
     * @return
     */
    public boolean existingAlbum(String s) {

        for (Album x : udata.getAlbums()) {
            if (x.getName().equals(s))
                return true;
        }
        return false;

    }

//    /**
//     * Saves the object data in a file
//     */
//    private void serialize() throws IOException {
//        FileOutputStream fileOut;
//        fileOut = openFileOutput(storeFile, Context.MODE_PRIVATE);
//        int i=0;
//        while(i< albums.size()) {
//            Album temp= albums.get(i);
////			System.out.println("writing user: "+ temp.getUsername());
//            try {
//                ObjectOutputStream out = new ObjectOutputStream(fileOut);
//                out.writeObject(temp);
//                System.out.println("saved: " + temp.getName());
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            i++;
//        }
//        fileOut.close();
//
//    }

//    public void writeNumAlbums() throws IOException {
//        String filename = numAlbumsFile;
//        String fileContents = numAlbums+"";
//        FileOutputStream outputStream;
//        try {
//            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
//            outputStream.write(fileContents.getBytes());
//            outputStream.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        System.out.println("wrote num albums (" + numAlbums +") to file");
//    }
//
//    /**
//     * loads number of users from text file and stores it in numUsers
//     * @throws IOException
//     */
//    public void loadNumAlbums() throws IOException {
//        FileInputStream in = openFileInput(numAlbumsFile);
//        InputStreamReader inputStreamReader = new InputStreamReader(in);
//        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//        StringBuilder sb = new StringBuilder();
//        String line;
//        while ((line = bufferedReader.readLine()) != null) {
//            sb.append(line);
//        }
//        String result = sb.toString();
//
//        inputStreamReader.close();
//        System.out.println("we got dis: " + result);
//        numAlbums=Integer.parseInt(result);
//   }
//
//    /**
//     * loads users back from serialized file
//     */
//    public void loadAlbums() {
//        Album temp = null;
//        try {
//            FileInputStream fileIn = openFileInput(storeFile);
////            FileInputStream fileIn = new FileInputStream(storeFile);
//            if(fileIn.getChannel().size() == 0) {
//                fileIn.close();
//                return;
//            }
//            int i=0;
//            while(i<numAlbums) {
//                ObjectInputStream in = new ObjectInputStream(fileIn);
//                temp = (Album) in.readObject();
//                System.out.println("Added Album: " + temp.getName());
//                albums.add(temp);
//                i++;
//            }
//            fileIn.close();
//        } catch (IOException e) {
////            e.printStackTrace();
//            System.out.println("***FILE NOT FOUND, INITALIZING ALBUMS");
//            albums = new ArrayList<Album>();
//            return;
//        } catch (ClassNotFoundException c) {
//            System.out.println("Class not found");
//            c.printStackTrace();
//            return;
//        }
//    }
//
//    public void saveData(){
//        try {
//            writeNumAlbums();
//            serialize();
//        } catch (java.io.IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public void loadData(){
//        try {
//            loadNumAlbums();
//            loadAlbums();
//        } catch (java.io.IOException e){
//            e.printStackTrace();
//        }
//
//    }


    public String[] packAlbumNames () {

        String[] names = new String[udata.getAlbums().size()];

        for ( int i = 0 ; i < names.length; i++ )
            names[i] = udata.getAlbums().get(i).getName();

        return names;

    }


    public void showAlbum(Album album, int position){
        System.out.println("ITEM CLICKED DAWG: " + album.getName());
        Bundle bundle = new Bundle();
        bundle.putString(ViewAlbumActivity.ALBUM_NAME, album.getName());
        bundle.putInt(ViewAlbumActivity.ALBUM_INDEX, position);
        Intent intent = new Intent(this, ViewAlbumActivity.class);
        intent.putExtras(bundle);
//        intent.putExtra("albumObj", new Gson().toJson(udata.getAlbums().get(position)));
//        intent.putExtra("albumList", packAlbumNames());
        startActivityForResult(intent,VIEW_ALBUM_CODE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (resultCode != RESULT_OK) {
            return;
        }

        Bundle bundle = intent.getExtras();
        if (bundle == null) {
            return;
        }
        if(intent!=null){
            // gather all info passed back by launched activity
            String albumName = bundle.getString(ViewAlbumActivity.ALBUM_NAME);
            int index = bundle.getInt(ViewAlbumActivity.ALBUM_INDEX);
            System.out.println("returned from [" + albumName + "], woo");
//            String jsonCurrentAlbum = bundle.getString("albumObj");

//            Album returnedFromAlbum = new Gson().fromJson(jsonCurrentAlbum, Album.class);
//            System.out.println("returned from " + returnedFromAlbum);
            try {
                udata = new UserData(context);
            } catch (IOException e) {
                System.out.println("error instantiating user data");
                e.printStackTrace();
            }
            udata.loadData();
//            udata.getAlbums().set(index,returnedFromAlbum);
//            udata.saveData();
            setAlbumsRecyclerView();
        } else {
            System.out.println("returned null somewhere idk");
        }


    }




    public void setAlbumsRecyclerView(){

        AlbumsAdapter.OnItemClickListener listener = new AlbumsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Album item, int position) {
                showAlbum(item, position);
            }
        };

        AlbumsAdapter adapter = new AlbumsAdapter(udata.getAlbums(), context, listener);
        albumsRV.setAdapter(adapter);
        albumsRV.setLayoutManager(new LinearLayoutManager(this));
        udata.saveData();
    }

    /**
     * for when the user swipes to delete album
     */
    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            // Row is swiped from recycler view
            // remove it from adapter
            int position = viewHolder.getAdapterPosition();
            udata.getAlbums().remove(position);
           udata.setNumAlbums(udata.getNumAlbums()-1);
           setAlbumsRecyclerView();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
           Drawable icon;
            final ColorDrawable background;
            icon = ContextCompat.getDrawable(context,
                    R.drawable.ic_action_delete_white);

            background = new ColorDrawable(Color.RED);
            // view the background view
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

            View itemView = viewHolder.itemView;
            int backgroundCornerOffset = 20; //so background is behind the rounded corners of itemView

            int iconMargin = (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconTop = itemView.getTop() + (itemView.getHeight() - icon.getIntrinsicHeight()) / 2;
            int iconBottom = iconTop + icon.getIntrinsicHeight();

            if (dX > 0) { // Swiping to the right
                int iconLeft = itemView.getLeft() + iconMargin + icon.getIntrinsicWidth();
                int iconRight = itemView.getLeft() + iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                background.setBounds(itemView.getLeft(), itemView.getTop(),
                        itemView.getLeft() + ((int) dX) + backgroundCornerOffset, itemView.getBottom());
            } else if (dX < 0) { // Swiping to the left
                int iconLeft = itemView.getRight() - iconMargin - icon.getIntrinsicWidth();
                int iconRight = itemView.getRight() - iconMargin;
                icon.setBounds(iconLeft, iconTop, iconRight, iconBottom);

                background.setBounds(itemView.getRight() + ((int) dX) - backgroundCornerOffset,
                        itemView.getTop(), itemView.getRight(), itemView.getBottom());
            } else { // view is unSwiped
                background.setBounds(0, 0, 0, 0);
            }

            background.draw(c);
            icon.draw(c);
        }
    };




}
